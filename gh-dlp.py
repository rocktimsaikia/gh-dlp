import json
import requests

owner = "rocktimsaikia"
repo = "anime-chan"
path = "/public"

# Todo:
# 1. List all the contents in a directory including the sub directories
def get_content():
    res = requests.get(f"https://api.github.com/repos/{owner}/{repo}/contents/{path}")
    data_list = res.json()
    new_data_list = []
    for data in data_list:
        new_data_list.append(
            {
                "name": data["name"],
                "type": data["type"],
                "url": data["url"],
                "git_url": data["git_url"],
                "download_url": data["download_url"],
            }
        )
    return new_data_list


def get_repo():
    res = requests.get(f"https://api.github.com/repos/{owner}/{repo}")
    data = res.json()
    return data


content = get_content()
repo = get_repo()

output = json.dumps(content, indent=4)
print(output)
